package main

type Db struct {
	Dialect string
	Dsn     string
}

type Bot struct {
	Token              string
	ChatId             int64
	Minutes            string
	QuickDeleteMinutes string
	YourCodeText       string
	WrongCodeText      string
	RegisterText       string
	CodeLength         int
	ResizeXMin         int
	ResizeXMax         int
	Emojis             string
	AdminCode          string
}

type Conf struct {
	Db  Db
	Bot Bot
}
