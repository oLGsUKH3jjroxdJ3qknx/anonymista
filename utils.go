package main

import (
	"crypto/rand"
	"github.com/kyokomi/emoji"
	"io"
	mathrand "math/rand"
	"net/http"
	"os"
	"strings"
	"time"
)

const codeChars = "abcdefghkmnprstwxz123456789"

// generate code
// @TODO: fetch code chars from configuration.json
func GenerateCode(length int) (string, error) {
	buffer := make([]byte, length)
	_, err := rand.Read(buffer)
	if err != nil {
		return "", err
	}

	codeCharsLength := len(codeChars)
	for i := 0; i < length; i++ {
		buffer[i] = codeChars[int(buffer[i])%codeCharsLength]
	}

	return string(buffer), nil
}

// get random emoji
func RandomEmoji(emojis string) string {
	mathrand.Seed(time.Now().UnixNano())
	emojiSplit := strings.Split(emojis, " ")
	return strings.Trim(emoji.Sprint(emojiSplit[mathrand.Intn(len(emojiSplit))]), " ")
}

// download file
func DownloadFile(filepath string, url string) error {
	// Get the data
	resp, err := http.Get(url)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	// Create the file
	out, err := os.Create(filepath)
	if err != nil {
		return err
	}
	defer out.Close()

	// Write the body to file
	_, err = io.Copy(out, resp.Body)
	return err
}
