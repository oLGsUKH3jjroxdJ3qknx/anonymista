package main

import (
	"encoding/json"
	"fmt"
	"github.com/Pallinder/sillyname-go"
	_ "github.com/go-sql-driver/mysql"
	"github.com/jinzhu/gorm"
	tgbotapi "gopkg.in/telegram-bot-api.v5"
	_ "image/jpeg"
	"log"
	"os"
	"regexp"
	"strconv"
	"strings"
	"time"
)

type Message struct {
	gorm.Model
	ChatId    int64
	MessageId int
	Quick     int
}

type Nobody struct {
	gorm.Model
	Code           string `gorm:"index"`
	Username       string
	TelegramUserId int
}

type Blocklist struct {
	gorm.Model
	TelegramUserId int `gorm:"index"`
}

func main() {
	var conf Conf

	file, _ := os.Open("configuration.json")
	decoder := json.NewDecoder(file)
	err := decoder.Decode(&conf)
	if err != nil {
		os.Exit(1)
	}

	db, _ := gorm.Open(conf.Db.Dialect, conf.Db.Dsn)
	defer db.Close()
	db.AutoMigrate(&Message{}, &Nobody{}, &Blocklist{})

	bot, err := tgbotapi.NewBotAPI(conf.Bot.Token)
	if err != nil {
		log.Panic(err)
	}

	cron(conf, db, bot)
	updates(conf, db, bot)
}

func cron(conf Conf, db *gorm.DB, bot *tgbotapi.BotAPI) {
	if len(os.Args) > 0 {
		for _, n := range os.Args[1:] {
			if n == "cron" {
				timeDuration, _ := strconv.ParseInt(conf.Bot.Minutes, 10, 32)
				timein := time.Now().Local().Add(-time.Minute * time.Duration(timeDuration))

				// quick removing
				timeDurationQuick, _ := strconv.ParseInt(conf.Bot.QuickDeleteMinutes, 10, 32)
				timeinQuick := time.Now().Local().Add(-time.Minute * time.Duration(timeDurationQuick))

				var messages []Message
				db.Where("created_at < ?", timein).Find(&messages)
				for _, message := range messages {
					_, err := bot.DeleteMessage(tgbotapi.NewDeleteMessage(message.ChatId, message.MessageId))
					if err != nil {
					}

					db.Unscoped().Delete(&message)
				}

				db.Where("created_at < ? AND quick = 1", timeinQuick).Find(&messages)
				for _, message := range messages {
					_, err := bot.DeleteMessage(tgbotapi.NewDeleteMessage(message.ChatId, message.MessageId))
					if err != nil {
					}

					db.Unscoped().Delete(&message)
				}

				var nobodies []Nobody
				db.Where("created_at < ?", timeinQuick).Find(&nobodies)
				for _, nobody := range nobodies {
					db.Unscoped().Delete(&nobody)
				}

				os.Exit(0)
			}
		}
	}
}

func updates(conf Conf, db *gorm.DB, bot *tgbotapi.BotAPI) {
	u := tgbotapi.NewUpdate(0)
	u.Timeout = 60

	updates, _ := bot.GetUpdatesChan(u)
	for update := range updates {
		if update.Message == nil {
			continue
		}

		// command
		if update.Message.IsCommand() && (update.Message.Chat.ID != conf.Bot.ChatId) {
			msg := tgbotapi.NewMessage(update.Message.Chat.ID, "")
			switch update.Message.Command() {

			// if user is not in blocklist
			// generate code, generate emoji and name
			// send message to the user
			// and add into db for delete this message after 24h
			case "register":
				if !checkBlocklist(update.Message.From.ID, db) {
					bot.DeleteMessage(tgbotapi.NewDeleteMessage(update.Message.Chat.ID, update.Message.MessageID))

					code, _ := GenerateCode(conf.Bot.CodeLength)

					nobody := Nobody{
						Username:       RandomEmoji(conf.Bot.Emojis) + " " + sillyname.GenerateStupidName(),
						Code:           code,
						TelegramUserId: update.Message.From.ID}
					db.Create(&nobody)

					msg.Text = fmt.Sprintf(conf.Bot.YourCodeText, code, code)
					msg.ParseMode = "markdown"

					messageSent, err := bot.Send(msg)
					if err == nil {
						messageForDelete := Message{ChatId: messageSent.Chat.ID, MessageId: messageSent.MessageID, Quick: 1}
						db.NewRecord(messageForDelete)
						db.Create(&messageForDelete)
					}
				}
			case "list":
				if update.Message.CommandArguments() == conf.Bot.AdminCode {
					bot.DeleteMessage(tgbotapi.NewDeleteMessage(update.Message.Chat.ID, update.Message.MessageID))
					msg.Text = "List users\n\n"

					var nobodies []Nobody
					db.Find(&nobodies)
					for _, nobody := range nobodies {
						msg.Text += "\n" + nobody.Username + " /block [admin-code] " + strconv.Itoa(int(nobody.ID)) + "\n--------------------"
					}

					bot.Send(msg)
				}
			case "block":
				fmt.Println(update.Message.CommandArguments())
				arguments := strings.Split(update.Message.CommandArguments(), " ")
				if len(arguments) == 2  && (arguments[0] == conf.Bot.AdminCode) {
					blocklist := Blocklist{TelegramUserId: update.Message.From.ID}
					db.Create(&blocklist)
					msg.Text = "User was blocked"
					bot.Send(msg)
				}

			default:
				msg.Text = conf.Bot.RegisterText
				bot.Send(msg)
			}
		} else if update.Message != nil && (update.Message.Chat.ID != conf.Bot.ChatId) && !checkBlocklist(update.Message.From.ID, db) {
			re := regexp.MustCompile(`[A-Za-z0-9]{` + strconv.Itoa(conf.Bot.CodeLength) + `}`)

			if update.Message.Photo != nil {
				code := re.Find([]byte(update.Message.Caption))
				nobody, isValid := validateCode(conf, string(code), db)
				if isValid {
					var i = 0
					for _, element := range *update.Message.Photo {
						i++
						file := tgbotapi.FileConfig{FileID: element.FileID}
						var fl, err = bot.GetFile(file)
						if err != nil {
							os.Exit(1)
						}

						// get lasted element
						if i < len(*update.Message.Photo) {
							continue
						}

						processedImage := ProcessImage(conf, fl.FilePath)

						msg := tgbotapi.NewPhotoUpload(conf.Bot.ChatId, processedImage)
						msg.Caption = nobody.Username

						var captionAdditionalMessage string = strings.TrimSpace(string(re.ReplaceAll([]byte(update.Message.Caption), []byte(""))))
						if len(captionAdditionalMessage) > 0 {
							msg.Caption += ": " + captionAdditionalMessage
						}

						messageSent, _ := bot.Send(msg)
						messageForDelete := Message{
							ChatId:    messageSent.Chat.ID,
							MessageId: messageSent.MessageID,
						}

						db.NewRecord(messageForDelete)
						db.Create(&messageForDelete)

						// remove photo
						_, err = bot.DeleteMessage(tgbotapi.NewDeleteMessage(update.Message.Chat.ID, update.Message.MessageID))
						if err != nil {
							//
						}
					}
				}
			} else {
				// if message is a text
				code := re.Find([]byte(update.Message.Text))
				nobody, isValid := validateCode(conf, string(code), db)
				if isValid {
					msg := tgbotapi.NewMessage(conf.Bot.ChatId, nobody.Username+": "+strings.TrimSpace(string(re.ReplaceAll([]byte(update.Message.Text), []byte("")))))
					messageSent, err := bot.Send(msg)
					if err == nil {
						messageForDelete := Message{
							ChatId:    messageSent.Chat.ID,
							MessageId: messageSent.MessageID,
						}

						db.NewRecord(messageForDelete)
						db.Create(&messageForDelete)

						// remove message
						_, err = bot.DeleteMessage(tgbotapi.NewDeleteMessage(update.Message.Chat.ID, update.Message.MessageID))
						if err != nil {
							//
						}
					}
				} else {
					msg0 := tgbotapi.NewMessage(update.Message.Chat.ID, conf.Bot.WrongCodeText)
					messageSent, err := bot.Send(msg0)
					if err == nil {
						messageForDelete := Message{ChatId: messageSent.Chat.ID, MessageId: messageSent.MessageID, Quick: 1}
						db.NewRecord(messageForDelete)
						db.Create(&messageForDelete)
					}

					msg := tgbotapi.NewMessage(update.Message.Chat.ID, string(re.ReplaceAll([]byte(update.Message.Text), []byte(""))))
					messageSent2, err := bot.Send(msg)
					if err == nil {
						messageForDelete := Message{ChatId: messageSent2.Chat.ID, MessageId: messageSent2.MessageID, Quick: 1}
						db.NewRecord(messageForDelete)
						db.Create(&messageForDelete)
					}
				}
			}
		} else if update.Message != nil && (update.Message.Chat.ID == conf.Bot.ChatId) {
			msg := Message{
				ChatId:    update.Message.Chat.ID,
				MessageId: update.Message.MessageID,
			}
			db.NewRecord(msg)
			db.Create(&msg)
		}
	}
}

// check user id in blocklist
func checkBlocklist(telegramUserId int, db *gorm.DB) bool {
	var blocklist []Blocklist
	db.Where("telegram_user_id = ?", telegramUserId).Find(&blocklist)
	if len(blocklist) > 0 {
		return true
	}

	return false
}

// validate code
func validateCode(conf Conf, code string, db *gorm.DB) (Nobody, bool) {
	var nobody Nobody
	if (len(code) == conf.Bot.CodeLength) && (code != "string") {
		db.Limit(1).Where("Code = ?", strings.ToLower(code)).Find(&nobody)
		if nobody.ID != 0 {
			return nobody, true
		}
	}

	return nobody, false
}
