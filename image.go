package main

import (
	"crypto/md5"
	"encoding/hex"
	"fmt"
	"github.com/nfnt/resize"
	tgbotapi "gopkg.in/telegram-bot-api.v5"
	"image/jpeg"
	"log"
	"math/rand"
	"os"
)

// process image: download, resize, return handler
func ProcessImage(conf Conf, filePath string) tgbotapi.FileReader {
	h := md5.New()
	fileName := hex.EncodeToString(h.Sum([]byte(filePath))[:])

	// download image from telegram
	err := DownloadFile(fileName, fmt.Sprintf(tgbotapi.FileEndpoint, conf.Bot.Token, filePath))
	if err != nil {
		log.Fatal(err)
	}

	// open downloaded image
	downloadedImage, err := os.Open(fileName)
	if err != nil {
		log.Fatal(err)
	}

	// decode image
	image, err := jpeg.Decode(downloadedImage)
	if err != nil {
		log.Fatal(err)
	}
	downloadedImage.Close()

	margin := rand.Intn(conf.Bot.ResizeXMax-conf.Bot.ResizeXMin) + conf.Bot.ResizeXMin
	m := resize.Resize(uint(image.Bounds().Max.X+margin), 0, image, resize.Lanczos3)

	out, err := os.Create(fileName + "_resized")
	if err != nil {
		log.Fatal(err)
	}
	defer out.Close()

	// write new image to file
	err = jpeg.Encode(out, m, nil)
	if err != nil {
		log.Fatal(err)
	}

	f, _ := os.Open(fileName + "_resized")
	b := tgbotapi.FileReader{Name: fileName, Reader: f, Size: -1}

	os.Remove(fileName)
	os.Remove(fileName + "_resized")

	return b
}
